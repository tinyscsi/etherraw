#include "utility/w5100.h"
#include "EtherRaw.h"

// XXX: don't make assumptions about the value of MAX_SOCK_NUM.
uint8_t EtherRawClass::_state[MAX_SOCK_NUM] = { 
  0, 0, 0, 0 };

void EtherRawClass::begin(uint8_t *mac_address)
{
  // Initialise the basic info
  W5100.init();
  SPI1.beginTransaction(SPI_ETHERNET_SETTINGS);
  W5100.setMACAddress(mac_address);
  SPI1.endTransaction();
}

void EtherRawClass::setMACAddress(uint8_t *mac_address)
{
  SPI1.beginTransaction(SPI_ETHERNET_SETTINGS);
  W5100.setMACAddress(mac_address);
  SPI1.endTransaction();
}

void EtherRawClass::getMACAddress(uint8_t *mac_address)
{
  SPI1.beginTransaction(SPI_ETHERNET_SETTINGS);
  W5100.getMACAddress(mac_address);
  SPI1.endTransaction();
}

EtherRawClass EtherRaw;
