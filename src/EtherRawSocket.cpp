#include "utility/w5100.h"
#include "utility/socket.h"

extern "C" {
  #include "string.h"
}

#include "Arduino.h"

#include "EtherRaw.h"
#include "EtherRawSocket.h"

EtherRawSocket::EtherRawSocket() : _sock(MAX_SOCK_NUM) {
}

EtherRawSocket::EtherRawSocket(uint8_t sock) : _sock(sock) {
}

int EtherRawSocket::open() {
  if (_sock != MAX_SOCK_NUM)
    return 0;

  // MACRAW only available on Socket 0
  uint8_t s = socketStatus(0);
  if (s == SnSR::CLOSED || s == SnSR::FIN_WAIT || s == SnSR::CLOSE_WAIT) {
    _sock = 0;
  }

  if (_sock == MAX_SOCK_NUM)
    return 0;

  socket(_sock, SnMR::MACRAW, 0, 0);

  while (status() != SnSR::MACRAW) {
    delay(1);
    if (status() == SnSR::CLOSED) {
      _sock = MAX_SOCK_NUM;
      return 0;
    }
  }

  return 1;
}

size_t EtherRawSocket::write(const uint8_t *buf, size_t size) {
  if (_sock == MAX_SOCK_NUM) {
    return 0;
  }
  if (!sendto(_sock, buf, size, NULL, 0)) {
    return 0;
  }
  return size;
}

int EtherRawSocket::available() {
  if (_sock != MAX_SOCK_NUM)
    return recvAvailable(_sock);
  return 0;
}

int EtherRawSocket::read(uint8_t *buf, size_t size) {
  return recvfrom(_sock, buf, size, NULL, NULL);
}

void EtherRawSocket::flush() {
  ::flush(_sock);
}

void EtherRawSocket::stop() {
  if (_sock == MAX_SOCK_NUM)
    return;

  // attempt to close the socket
  close(_sock);

  _sock = MAX_SOCK_NUM;
}

uint8_t EtherRawSocket::connected() {
  if (_sock == MAX_SOCK_NUM) return 0;

  uint8_t s = status();
  return !(s == SnSR::LISTEN || s == SnSR::CLOSED || s == SnSR::FIN_WAIT ||
    (s == SnSR::CLOSE_WAIT && !available()));
}

uint8_t EtherRawSocket::status() {
  if (_sock == MAX_SOCK_NUM) return SnSR::CLOSED;
  return socketStatus(_sock);
}

// the next function allows us to use the client returned by
// EtherRawSocket::available() as the condition in an if-statement.

EtherRawSocket::operator bool() {
  return _sock != MAX_SOCK_NUM;
}

bool EtherRawSocket::operator==(const EtherRawSocket& rhs) {
  return _sock == rhs._sock && _sock != MAX_SOCK_NUM && rhs._sock != MAX_SOCK_NUM;
}

uint8_t EtherRawSocket::getSocketNumber() {
  return _sock;
}
