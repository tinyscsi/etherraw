#ifndef etherrawsocket_h
#define etherrawsocket_h
#include "Arduino.h"	

class EtherRawSocket {

public:
  EtherRawSocket();
  EtherRawSocket(uint8_t sock);

  uint8_t status();
  virtual int open();
  virtual size_t write(const uint8_t *buf, size_t size);
  virtual int available();
  virtual int read(uint8_t *buf, size_t size);
  virtual void flush();
  virtual void stop();
  virtual uint8_t connected();
  virtual operator bool();
  virtual bool operator==(const bool value) { return bool() == value; }
  virtual bool operator!=(const bool value) { return bool() != value; }
  virtual bool operator==(const EtherRawSocket&);
  virtual bool operator!=(const EtherRawSocket& rhs) { return !this->operator==(rhs); };
  uint8_t getSocketNumber();

private:
  uint8_t _sock;
};

#endif
