#ifndef etherraw_h
#define etherraw_h

#include <inttypes.h>
//#include "w5100.h"
#include "EtherRawSocket.h"

#define MAX_SOCK_NUM 4

class EtherRawClass {
public:
  static uint8_t _state[MAX_SOCK_NUM];
  // Initialise the Ethernet shield to use the provided MAC address
  void begin(uint8_t *mac_address);
  void setMACAddress(uint8_t *mac_address);
  void getMACAddress(uint8_t *mac_address);

  friend class EtherRawSocket;
};

extern EtherRawClass EtherRaw;

#endif
